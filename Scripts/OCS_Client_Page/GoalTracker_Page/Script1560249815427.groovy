import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('OCS_Client_Page/Login_Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('OCS_HomePage/Page_OCS/p_Track My Plan Goals'))

WebUI.click(findTestObject('Goals/Page_OCS/a_Goal Tracker'))

WebUI.click(findTestObject('Goals/Page_OCS/a_Goal History'))

WebUI.click(findTestObject('Goals/Page_OCS/a_Add New Goal'))

WebUI.verifyTextPresent('New Goal Details:', false)

WebUI.setText(findTestObject('Goals/Page_OCS/input_Goal_goal'), 'Demo test')

WebUI.click(findTestObject('Goals/Page_OCS/input_Start Date_start_date'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Goals/Page_OCS/Page_OCS/div_11'))

WebUI.click(findTestObject('Goals/Page_OCS/input_End Date_end_date'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Goals/Page_OCS/Page_OCS/div_18'))

WebUI.click(findTestObject('Goals/Page_OCS/button_Submit'))

WebUI.verifyElementPresent(findTestObject('Goals/Page_OCS/div_Participant Goal submit successfully'), 1)

