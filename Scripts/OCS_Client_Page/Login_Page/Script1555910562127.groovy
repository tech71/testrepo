import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.verifyElementVisible(findTestObject('Object Repository/Ocs_client_login/Page_OCS/img_Please Login Below_img-fluid'))

if (true) {
    WebUI.verifyElementPresent(findTestObject('Object Repository/Ocs_client_login/Page_OCS/div_Welcome BackPlease Login Below'), 
        10)

    WebUI.setText(findTestObject('Object Repository/Ocs_client_login/Page_OCS/input_Welcome Back_username'), 'archana123')

    WebUI.setText(findTestObject('Ocs_client_login/Page_OCS/input_Welcome Back_password'), 'Archana@123')

    WebUI.click(findTestObject('Object Repository/Ocs_client_login/Page_OCS/button_Submit'))
} else {
    WebUI.click(findTestObject('Ocs_client_login/Page_OCS/a_Forgotten your Password'))

    WebUI.setText(findTestObject('Object Repository/Ocs_client_login/Page_OCS/input_Forgot Password_username'), 'archana123')

    WebUI.click(findTestObject('Object Repository/Ocs_client_login/Page_OCS/button_Request'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/Ocs_client_login/Page_OCS/div_Please visit your mail inbox to reset your password'))
}

