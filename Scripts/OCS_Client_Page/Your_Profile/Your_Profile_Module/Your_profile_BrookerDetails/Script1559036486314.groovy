import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('OCS_Client_Page/Login_Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Your_Prof_Details/Page_OCS/p_Update My Prole'))

WebUI.click(findTestObject('brooker/Page_OCS/i_Update_icon icon-update1-ie updte_ics'))

WebUI.setText(findTestObject('brooker/Page_OCS/input_First Name_firstname0'), 'test')

WebUI.setText(findTestObject('brooker/Page_OCS/input_Last Name_lastname0'), 'demo')

not_run: WebUI.click(findTestObject('brooker/Page_OCS/div_Contact'))

not_run: WebUI.click(findTestObject('brooker/Page_OCS/div_Sister'))

WebUI.setText(findTestObject('brooker/Page_OCS/input_Contact Number_phone0'), '56441233216')

WebUI.setText(findTestObject('brooker/Page_OCS/input_Email id_email0'), 'test@gmail.com ')

WebUI.click(findTestObject('brooker/Page_OCS/button_Submit'))

not_run: WebUI.click(findTestObject('address/Page_OCS/i_Update_icon icon-update1-ie updte_ics'))

not_run: WebUI.click(findTestObject('address/Page_OCS/button_Submit'))

