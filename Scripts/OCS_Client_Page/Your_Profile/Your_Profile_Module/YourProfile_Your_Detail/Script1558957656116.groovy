import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('OCS_Client_Page/Login_Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Your_Prof_Details/Page_OCS/p_Update My Prole'))

WebUI.click(findTestObject('Your_Prof_Details/Page_OCS/Page_OCS/i_Update_icon icon-update1-ie updte_ics'))

WebUI.setText(findTestObject('Your_Prof_Details/Page_OCS/input_Preferred Name_preferredname'), '84654')

WebUI.setText(findTestObject('Your_Prof_Details/Page_OCS/input_NDIS _NDISNO'), '5632132')

WebUI.setText(findTestObject('Your_Prof_Details/Page_OCS/input_Medicare_medicare'), 'uihrtjiggioh')

WebUI.setText(findTestObject('Your_Prof_Details/Page_OCS/input_CRN_CRN'), '541342')

WebUI.click(findTestObject('Your_Prof_Details/Page_OCS/button_Submit'))

WebUI.verifyElementPresent(findTestObject('Your_Prof_Details/Page_OCS/div_Profile is submitted for admin approval'), 5)

