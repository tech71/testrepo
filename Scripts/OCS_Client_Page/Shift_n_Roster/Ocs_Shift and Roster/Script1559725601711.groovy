import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('OCS_Client_Page/Login_Page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('sift and roster/Page_OCS/p_View Upcoming  Book'))

WebUI.click(findTestObject('sift and roster/Page_OCS/h4_My ONCALL Calendar'))

WebUI.click(findTestObject('sift and roster/Page_OCS/h1_My ONCALL Calendar'))

WebUI.click(findTestObject('sift and roster/Page_OCS/i_My ONCALL Calendar_icon icon-back-arrow'))

WebUI.click(findTestObject('sift and roster/Page_OCS/h4_Book Shift'))

WebUI.click(findTestObject('sift and roster/Page_OCS/input_Start Date Time_start_time'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Goals/Page_OCS/Page_OCS/Page_OCS/div_29'))

WebUI.click(findTestObject('Goals/Page_OCS/Page_OCS/Page_OCS/li_1015 PM'))

WebUI.click(findTestObject('sift and roster/Page_OCS/input_End Date Time_end_time'))

WebUI.click(findTestObject('Goals/Page_OCS/Page_OCS/Page_OCS/div_29'))

WebUI.click(findTestObject('Goals/Page_OCS/Page_OCS/Page_OCS/li_1145 PM'))

WebUI.setText(findTestObject('sift and roster/Page_OCS/input_Location_location0'), 'ATC,Pearce')

WebUI.click(findTestObject('sift and roster/Page_OCS/div_Please Select'))

WebUI.click(findTestObject('sift and roster/Page_OCS/div_ACT'))

WebUI.click(findTestObject('sift and roster/Page_OCS/div_Suburb_Select-value'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('sift and roster/Page_OCS/Page_OCS/Page_OCS/input'), 'arc')

WebUI.delay(2)

WebUI.click(findTestObject('sift and roster/Page_OCS/div_Pearce'))

WebUI.click(findTestObject('Object Repository/sift and roster/Page_OCS/button_Create Shift'))

WebUI.verifyElementVisible(findTestObject('sift and roster/Page_OCS/Page_OCS/div_Your shift is waiting for approval'), FailureHandling.STOP_ON_FAILURE)

