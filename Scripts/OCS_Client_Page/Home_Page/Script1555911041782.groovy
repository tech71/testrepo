import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('OCS_Client_Page/Login_Page'), [:], FailureHandling.STOP_ON_FAILURE)

if (true) {
    WebUI.verifyElementPresent(findTestObject('OCS_HomePage/Page_OCS/h1_Hi archana'), 2)

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_Update My Prole'))

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_View Upcoming  Book'))

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_Track My Plan Spending'))

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_Track My Plan Goals'))

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_View  Send Mail'))

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_Provide  Track Feedback'))

    WebUI.verifyElementClickable(findTestObject('OCS_HomePage/Page_OCS/p_Manage My Settings'))
}

