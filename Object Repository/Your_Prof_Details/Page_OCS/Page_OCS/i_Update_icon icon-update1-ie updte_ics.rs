<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Update_icon icon-update1-ie updte_ics</name>
   <tag></tag>
   <elementGuidId>bf3ba944-b7c3-424d-9e56-437845703af6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'col-lg-2 align-self-end']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='collapseOne']/div/div[2]/a/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-2 align-self-end</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;collapseOne&quot;)/div[@class=&quot;row Block_row&quot;]/div[@class=&quot;col-lg-2 align-self-end&quot;]/a[@class=&quot;update_but pb-3 pr-3 flx_upd1&quot;]/i[@class=&quot;icon icon-update1-ie updte_ics&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='collapseOne']/div/div[2]/a/i</value>
   </webElementXpaths>
</WebElementEntity>
