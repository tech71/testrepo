<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Ocs_Client_Portal</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>06dd3158-f30a-433f-be06-7f727a05f339</testSuiteGuid>
   <testCaseLink>
      <guid>e63351cb-45d6-4636-b59f-0979317f2045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_FeedBackPage_Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b3345d5-c7ae-40bd-a31b-99ee585621ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_GoalTraker_Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>736b989d-3cd7-4983-8851-99676952dfa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_HomePage_test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d809e34-b700-4ac2-beab-75932f7f91db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_Login_test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2b3383c-9bb4-430e-ac80-1e03dbea2fb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_Massage_N_NotificationPage_Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fc9a9d2-9806-4d3f-be4e-fbf770cef48e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_ShiftNRoster</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>763e59a7-63aa-459a-a62b-e1c8d874eeb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OCS_Client_Test/Ocs_YourProfile</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
